---
date: 2022-04-30
title: Tasks in April 2022
service: cloud
---

## Service Canvas available in Plony
Service Providers can now provide all service information asked for during the Onboarding process in [Plony](https://plony.helmholtz-berlin.de/), our data management system based on Plone.

