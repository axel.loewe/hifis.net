---
date: 2022-01-25
title: Tasks in Jan 2022
service: cloud
---

## User-specific Service View in Cloud Portal
In Cloud Portal, logged-in users shall be able to have a specific view on the services they are allowed to use, as well as service usage conditions. Also, a user can see and manage their relevant service notifications.  
(This work package was postponed from September 2021 to January 2022, as no user-specific information will be available by then.)
