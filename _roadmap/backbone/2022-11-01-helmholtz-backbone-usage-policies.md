---
date: 2022-11-01
title: Tasks in November 2022
service: backbone
---

## Helmholtz Backbone Network: Usage Policies
For 2022, a set of policy rules, regulating the basic participation conditions of the Helmholtz Backbone network,
shall be worked out and put into action for the participants.
