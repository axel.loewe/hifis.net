---
date: 2021-12-01
title: Tasks in December 2021
service: backbone
---

## AAI: Deprovisioning information to services
Obtained deprovisioning information shall be provided to services. This requires cooperation with the
Service Integration working group of the Cloud Cluster.




