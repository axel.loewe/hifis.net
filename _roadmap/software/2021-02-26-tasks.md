---
date: 2021-02-26
title: Tasks in February 2021
service: software
---

## Publish training program for the first half of 2021 
The education and training work package publishes the offered training events for the first half of 2021 on <{% link events.md %}>.
