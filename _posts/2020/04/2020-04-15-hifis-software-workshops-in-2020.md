---
title: "HIFIS Software Workshops in 2020"
title_image: default
date: 2020-04-15
authors:
  - huste
layout: blogpost
categories:
  - news
excerpt:
  "HIFIS workshop cluster in spring and autumn 2020: Basic Software Carpentry,
  Introduction to GitLab and Bring Your Own Script. Specialized workshops
  are being planned: Catch them all by keeping an eye on our events page!"
lang: en
lang_ref: 2020-04-15-hifis-software-workshops-in-2020
---

Several different software development trainings will be offered to researchers.

- _Software Carpentry_ workshops introduce scientists to use- and powerful
  tools: Shell, Python and R for effective and reproducible scientific
  programming, and Git for version controlling your projects.
- _Bring Your Own Script_ workshops
  ([like on May 12th and 13th]({% link _events/2020/2020-05-12-ready-script-for-publication.md %}))
  will help you to make your code publication-ready:
  Regardless of your preferred programming language, we will advise you on all
  aspects of making your research software (re)usable for others,
  e.g. how to add a license.
- _GitLab for Software Development in Teams_ introduces advanced GitLab features:
  GitLab’s project management, collaboration and automation features will help
  software development teams to improve software quality and speed up their
  release cycles.
  Join us on
  [June 9th and 10th]({% link _events/2020/2020-06-09-GitLab-Software-Development-Teams.md %})!

The [HIFIS Software events]({% link events.md %}) page provides
your with more details and keeps you updated about future events.
More specialized events outside this core curriculum —
like an
[Introduction to Snakemake]({% link _events/2019/2019-12-11-snakemake-introduction-workshop.md %}) —
are being planned, so best keep an eye on that page!
