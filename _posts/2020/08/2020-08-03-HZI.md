---
title: "HZI-HIFIS collaboration for Coronavirus Surveys"
title_image: mitchell-luo-H3htK85wwnU-unsplash.jpg
date: 2020-08-03
authors:
  - jandt
layout: blogpost
categories:
  - news
  - Use Case
redirect_from:
  - news/2020/08/03/HZI
excerpt_separator: <!--more-->
---

The [German Center for Infection Research (DZIF)](https://www.dzif.de) builds a platform collecting methodologies and results of coronavirus antibody studies. 
In cooperation with HIFIS and led by scientists at the [Helmholtz Centre for Infection Research (HZI)](https://www.helmholtz-hzi.de), the project "LEOSS.sero-survey" is being implemented.
<!--more-->

#### The LEOSS.sero-survey platform
The envisioned platform offers research groups the possibility to jointly evaluate anonymised data from various studies of different origins. 
This helps to increase statistical significance and study robustness. 
The approach drives the need for a commonly accessible data management. 
Virtual resources of the Helmholtz Association can be used for that. 
In this context, a data management system is being provided as a pilot project within HIFIS 
by the [Helmholtz-Zentrum Berlin (HZB)](https://www.helmholtz-berlin.de/).

#### Invitation for contribution
Interested researchers and centers are invited to participate in the LEOSS.sero-survey. Please visit the [**SeroHub Portal**](https://serohub.net/en/) for further information and getting started.

<a type="button" class="btn btn-outline-primary btn-lg" href="https://www.dzif.de/en/how-many-people-are-now-immune-sars-cov-2">
  <i class="fas fa-external-link-alt"></i> Read more
</a>


