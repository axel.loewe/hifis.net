---
title: "Update of the HIFIS Workshop Program 2021"
title_image: default
date: 2021-06-30
authors:
  - schlauch
layout: blogpost
tags:
  - workshops
  - announcement
  - research software
categories:
  - news
excerpt: The HIFIS Software Workshop Program for the second half 2021 has been updated.

---

We are happy to announce the updated **HIFIS Workshop Program 2021**.
The workshops cover various research software development related topics such as Git, GitLab, Python programming, team software development and research software publication.  

As usual, you can find all workshops on [the HIFIS events page](https://hifis.net/events).
The registration usually opens **2 to 3 weeks before** the workshop. The exact dates are given on the individual workshop sites.
We are looking forward to see you in one of our next workshops!

---

Cannot find exactly the workshop you are looking for? Talk to us directly:
{% include contact_us/mini.html %}
{% include helpdesk/mini.html %}
