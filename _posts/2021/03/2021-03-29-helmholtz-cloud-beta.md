---
title: "Helmholtz Cloud and Cloud Portal (beta) online"
title_image: spacex-uj3hvdfQujI-unsplash.jpg
date: 2021-03-29
authors:
  - jandt
layout: blogpost
categories:
  - news
redirect_from:
  - cloud-platform
excerpt: >
    The first Helmholtz Cloud services have been published and are accessible via the new Cloud Portal.
    Check out the new services!
    More services will be integrated soon.

---

# Helmholtz Cloud (beta)

The first Helmholtz Cloud services have been published and are accessible via the new [**Cloud Portal**](https://helmholtz.cloud).
Check out the new services!

Our Services cover many popular software solutions:

* **Gitlab** DevOps platform
* **JupyterHub** Server for Jupyter Notebooks
* **Mattermost** Chat Service
* **Nextcloud** Sync&Share Platform
* **OpenStack** Cloud Computing Platform
* **Zammad** Ticketing System

Further services are being integrated continuously.

*Note that this is a preliminary/pilot version of the Helmholtz Cloud and Cloud Portal.
Be also aware that not all services are accessible to all users; please check service details.*

## Access using Helmholtz AAI

Users from [almost all Helmholtz centres](https://hifis.net/doc/backbone-aai/list-of-connected-centers/) can use these services by logging in with their home institution's username and passphrase.
This is achieved based on the [Helmholtz Authentication and Authorisation Infrastructure](https://aai.helmholtz.de) (AAI) 
[established in 2020]({% post_url 2020/07/2020-07-24-helmholtz-aai %}).

Once logged in to one service, you may notice that you will gain immediate access to some of the other services without repeated login.
Welcome to Single-Sign On (SSO), another feature of Helmholtz AAI!


#### Non-Helmholtz users
Helmholtz AAI supports authentication using a "social" account. Currently, ORCID, Google and GitHub are supported. This allows non-Helmholtz users to use our services.

Non-Helmholtz users cannot use Helmholtz services automatically. They need to be authorised to use a service by joining a Virtual Organization (VO). As a Helmholtz employee, you may request a VO and invite the other participants to join. Once your VO has been established, you will need to negotiate with service providers to support your VO.
Please check the [Guideline for VO Administrators](https://hifis.net/doc/backbone-aai/guidelines-vos/) on how to do this.



## Service selection and further integration

The provided services are part of the **Initial Service Portfolio** that had been [announced earlier]({% post_url 2020/10/2020-10-13-initial-service-portfolio %}). 

More services will be integrated soon!
To not miss any updates on this and other HIFIS progress:

<a href="https://lists.desy.de/sympa/subscribe/hifis-announce" 
                            class="btn btn-outline-secondary"
                            title="Subscribe to HIFIS Announcements Letter - also unsubscribe"> 
                            Subscribe to HIFIS Announcement letter <i class="fas fa-bell"></i></a>
