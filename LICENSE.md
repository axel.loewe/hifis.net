---
title: License
author: the HIFIS team
layout: default
---


# License Information

Different licenses apply for specific parts of the project.
The full text of each license can be found in the location specified in the appropriate section of each document.

This file gives an overview which license applies to which part of the project.

## Proprietary
The following items are copyright protected.

  * The Helmholtz logo and the HIFIS logo
  * The website corporate design
  * The _CorporateS_ and _Hermann_ fonts

© by Helmholtz-Gemeinschaft Deutscher Forschungszentren e.V. <br/>
Helmholtz Association of German Research Centres

All rights reserved.

## GNU GPL 3
Source code posted on this website is licensed under GNU GPLv3 unless explicitly 
stated otherwise.
[License text](https://www.gnu.org/licenses/gpl-3.0.html)

## MIT License
The file _normalize.css_ is provided by a third party and comes under 
MIT License. Please refer to the file itself for origin, version and 
license information.

## Creative Commons CC-BY 4.0
Any content not listed above defaults to the 
[![Creative Commons License Agreement](https://i.creativecommons.org/l/by/4.0/80x15.png "CC-BY 4.0")](https://creativecommons.org/licenses/by/4.0/)
_Creative Commons Attribution 4.0 International License_ 
unless explicitly stated otherwise.
